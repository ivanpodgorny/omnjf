'use strict';

module.exports.clone = require('./lib/clone');
module.exports.has = require('./lib/has');
module.exports.merge = require('./lib/merge');
