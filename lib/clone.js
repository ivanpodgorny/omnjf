'use strict';

const _clone = require('lodash/cloneDeep');

let clone = targetObj => _clone(targetObj);

module.exports = clone;
