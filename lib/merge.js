'use strict';

const _clone = require('lodash/cloneDeep');

function merge(targetObj, sourceObj, propArr) {
  let targetClone = _clone(targetObj);

  for (let i = 0; i < propArr.length; i++) {
    let propName = propArr[i];

    if (!sourceObj || !sourceObj[propName]) continue;

    switch (typeof targetClone[propName]) {
      case 'string':
        targetClone[propName] = sourceObj[propName] + ' ' + targetClone[propName];
        break;
      case 'object':
        Object.assign(targetClone[propName], sourceObj[propName]);
        break;
    }
  }

  return targetClone;
}

module.exports = merge;
