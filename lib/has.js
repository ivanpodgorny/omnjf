'use strict';

let has = (str, substr) => ~str.indexOf(substr);

module.exports = has;
